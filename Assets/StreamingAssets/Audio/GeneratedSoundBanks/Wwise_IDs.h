/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AQUARIUM = 456876952U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID ROOM_TONE = 1272037051U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_SURFACE_CARPET = 117396797U;
                static const AkUniqueID SWITCH_SURFACE_CONCRETE = 3294452117U;
                static const AkUniqueID SWITCH_SURFACE_TILE = 2754391064U;
                static const AkUniqueID SWITCH_SURFACE_WOOD = 156175339U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB_INDOOR = 3997975935U;
        static const AkUniqueID AMB_OUTSIDE = 1013211851U;
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
